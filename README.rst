1. ``mkdir msg_board && cd msg_board``
2. ``git clone https://bitbucket.org/sahchos/test_light``
3. ``virtualenv -p python3 venv``
4. ``source venv/bin/activate && cd test_light/``
5. ``pip install -r requirements/local.txt``
6. Rename ``env.example`` to ``.env`` and setup variables. IMPORTANT change DB settings in ``.env`` file. You can leave "social auth" settings as is.
7. ``python manage.py migrate``
8. ``python manage.py local_social_accounts``
9. ``python manage.py runserver``
10. http://127.0.0.1:8000

|
Or run it with Docker
=====================

1. ``git clone https://bitbucket.org/sahchos/test_light``
2. ``mv test_light/env.docker_example test_light/.env``
3. ``cd test_light/docker_files && sudo docker-compose up``
4. http://127.0.0.1:8000
