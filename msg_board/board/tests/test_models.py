from test_plus.test import TestCase

from ..models import Message, Comment


class TestMessage(TestCase):
    def setUp(self):
        self.user = self.make_user()
        self.message = Message.objects.create(text='Test message for test', created_by=self.user)

    def test__str__(self):
        self.assertEqual(self.message.__str__(), 'Message Test messa')


class TestComment(TestCase):
    def setUp(self):
        self.user = self.make_user()
        self.message = Message.objects.create(text='Test message for test', created_by=self.user)
        self.comment = Comment.objects.create(text='Test comment for test', created_by=self.user, msg=self.message)

    def test__str__(self):
        self.assertEqual(self.comment.__str__(), 'Comment Test comme')
