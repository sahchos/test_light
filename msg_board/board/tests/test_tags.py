from test_plus.test import TestCase

from django.template.loader import render_to_string

from ..templatetags import board_tags
from ..models import Message, Comment


class RenderMsgTagTest(TestCase):

    def test_msg_thread_rendered(self):
        user = self.make_user()
        message = Message.objects.create(text='First msg', created_by=user)
        comment = Comment.objects.create(text='Msg comment', created_by=user, msg=message)
        comment2 = Comment.objects.create(text='Comment to comment', created_by=user, msg=message,
                                          comment=comment)

        comments_all = {
            'by_message': {
                message.pk: [comment]
            },
            'by_comment': {
                comment.pk: [comment2]
            }
        }
        rendered = render_to_string(
            'snippets/msg.html',
            board_tags.render_msg({'comments_all': comments_all}, message, comments_all['by_message'][message.pk])
        )

        self.assertIn('First msg', rendered)
        self.assertIn('Msg comment', rendered)
        self.assertIn('Comment to comment', rendered)


class ClassnameTagTest(TestCase):

    def test_classname(self):
        user = self.make_user()
        message = Message.objects.create(text='First msg', created_by=user)
        comment = Comment.objects.create(text='Msg comment', created_by=user, msg=message)
        self.assertEqual(board_tags.classname(message), 'Message')
        self.assertEqual(board_tags.classname(comment), 'Comment')


class GetTagTest(TestCase):

    def test_get(self):
        d = {'key': 'val'}
        self.assertEqual(board_tags.get(d, 'key'), 'val')
        self.assertIsNone(board_tags.get(d, 'fake_key'))
