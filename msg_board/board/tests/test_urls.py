from test_plus.test import TestCase

from ..models import Message, Comment


class TestBoardURLs(TestCase):
    """Test URL patterns for board app."""

    def setUp(self):
        self.user = self.make_user()
        self.message = Message.objects.create(text='Test message for test', created_by=self.user)
        self.comment = Comment.objects.create(text='Test comment for test', created_by=self.user, msg=self.message)

    def test_home_reverse(self):
        self.assertEqual(self.reverse('board:home'), '/')

    def test_message_add_reverse(self):
        self.assertEqual(self.reverse('board:message_add'), '/message/add/')

    def test_message_edit_reverse(self):
        self.assertEqual(
            self.reverse('board:message_edit', pk=self.message.pk), '/message/{}/edit/'.format(self.message.pk)
        )

    def test_comment_add_reverse(self):
        self.assertEqual(self.reverse('board:comment_add'), '/comment/add/')

    def test_comment_edit_reverse(self):
        self.assertEqual(
            self.reverse('board:comment_edit', pk=self.comment.pk), '/comment/{}/edit/'.format(self.comment.pk)
        )
