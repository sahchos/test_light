from test_plus.test import TestCase

from crispy_forms.layout import Layout, Submit

from ..forms import MessageForm, CommentForm
from ..models import Message, Comment


class TestMessageForm(TestCase):

    def test__init__(self):
        form = MessageForm()
        self.assertEqual(form.helper.form_class, 'form-inline tac')
        self.assertEqual(form.helper.form_action, self.reverse('board:message_add'))

        layout = Layout(
            'text',
            Submit('submit', 'Send', css_class='btn btn-primary'),
        )
        self.assertEqual(len(form.helper.layout), len(layout))
        self.assertListEqual(form.helper.layout.get_field_names(), layout.get_field_names())


class TestCommentForm(TestCase):

    def setUp(self):
        self.user = self.make_user()
        self.message1 = Message.objects.create(text='Test message for test', created_by=self.user)
        self.message2 = Message.objects.create(text='Test message for test', created_by=self.user)
        self.comment = Comment.objects.create(text='Test comment for test', created_by=self.user, msg=self.message1)

    def test_clean_success(self):
        form_init = {
            'text': 'Test comment',
            'msg': self.message1.pk,
        }
        form = CommentForm(form_init)
        self.assertTrue(form.is_valid())

        # Run the actual clean method
        cleaned_data = form.clean()
        form_cleaned_data = {
            'text': cleaned_data['text'],
            'msg': cleaned_data['msg'].pk,
        }
        self.assertDictEqual(form_cleaned_data, form_init)

    def test_clean_fail(self):
        # wrong msg(thread) for comment
        form_init = {
            'text': 'Test comment',
            'msg': self.message2.pk,
            'comment': self.comment.pk
        }
        form = CommentForm(form_init)
        self.assertFalse(form.is_valid())

        self.assertTrue(len(form.errors) == 1)
        self.assertIn('Incorrect data', form.non_field_errors().as_text())
