from test_plus.test import CBVTestCase, TestCase

from django.conf import settings
from django.test.client import RequestFactory

from ..models import Message, Comment
from ...board import views
from ..forms import MessageForm


class MessageListViewTest(CBVTestCase):

    def setUp(self):
        self.factory = RequestFactory()
        self.user = self.make_user()
        self.view = self.get_instance(views.MessageListView)

    def test_context_data(self):
        Message.objects.bulk_create(
            [Message(text='{} message for test'.format(i), created_by=self.user) for i in range(20)]
        )

        msgs = Message.objects.all().order_by('-id')[:3][::-1]

        comment1 = Comment.objects.create(text='Comment 1', created_by=self.user, msg=msgs[0])
        comment1_1 = Comment.objects.create(text='Comment 1_1', created_by=self.user, msg=msgs[0],
                                            comment=comment1)
        comment1_2 = Comment.objects.create(text='Comment 1_2', created_by=self.user, msg=msgs[0],
                                            comment=comment1)
        comment1_1_1 = Comment.objects.create(text='Comment 1_1_1', created_by=self.user, msg=msgs[0],
                                              comment=comment1_1)

        comment2 = Comment.objects.create(text='Comment 2', created_by=self.user, msg=msgs[1])
        comment2_1 = Comment.objects.create(text='Comment 1_1', created_by=self.user, msg=msgs[1],
                                            comment=comment2)

        comment3 = Comment.objects.create(text='Comment 3', created_by=self.user, msg=msgs[2])

        comments_all = {
            'by_message': {
                msgs[0].pk: [comment1],
                msgs[1].pk: [comment2],
                msgs[2].pk: [comment3]
            },
            'by_comment': {
                comment1.pk: [comment1_1, comment1_2], # test order by comments. old first
                comment1_1.pk: [comment1_1_1],
                comment2.pk: [comment2_1]
            }
        }

        self.view.object_list = self.view.get_queryset()
        self.view.request = self.factory.get(self.reverse('board:home'))
        context = self.view.get_context_data()

        self.assertIsInstance(context['form'], MessageForm)
        self.assertEqual(len(context['msgs']), settings.MESSAGE_PER_PAGE)
        self.assertDictEqual(comments_all, context['comments_all'])

    def test_get_query_set(self):
        Message.objects.bulk_create(
            [Message(text='{} message for test'.format(i), created_by=self.user) for i in range(20)]
        )

        res = self.view.get_queryset()

        self.assertNumQueries(1)
        self.assertEqual(len(res), 20)
        self.assertGreater(res[0].date_created, res[19].date_created)


class MessageCreateViewTest(TestCase):

    def setUp(self):
        self.user = self.make_user()

    def test_get_success_url(self):
        view = CBVTestCase().get_instance(views.MessageCreateView)
        self.assertEqual(view.get_success_url(), self.reverse('board:home'))

    def test_login_required(self):
        self.assertLoginRequired('board:message_add')

    def test_form_invalid(self):
        with self.login(self.user):
            response = self.post(self.reverse('board:message_add'), data={
                'text': ''
            })
            self.assertRedirects(response, self.reverse('board:home'))

        msgs = Message.objects.count()
        self.assertEqual(msgs, 0)

    def test_form_valid(self):
        self.assertEqual(Message.objects.count(), 0)

        with self.login(self.user):
            response = self.post(self.reverse('board:message_add'), data={
                'text': 'Create msg',
                'created_by': self.user
            })
            self.assertRedirects(response, self.reverse('board:home'))

        self.assertEqual(Message.objects.count(), 1)


class AjaxableResponseMixinTest(TestCase):
    def test_form_invalid(self):
        user = self.make_user()
        with self.login(user):
            response = self.post(
                self.reverse('board:comment_add'),
                data={'text': 'Test'},
                extra={'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertEqual(response.status_code, 400)
            self.assertIn('msg', str(response.content))


class MessageUpdateViewTest(TestCase):
    def setUp(self):
        self.user = self.make_user()
        self.user2 = self.make_user('user2')
        self.message = Message.objects.create(text="Old text", created_by=self.user)

    def test_get_success_url(self):
        view = CBVTestCase().get_instance(views.MessageUpdateView)
        self.assertEqual(view.get_success_url(), self.reverse('board:home'))

    def test_wrong_user(self):
        with self.login(self.user2):
            response = self.post(
                self.reverse('board:message_edit', pk=self.message.pk),
                data={'text': 'New text'},
                extra={'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertEqual(response.status_code, 404)

    def test_form_valid(self):
        with self.login(self.user):
            response = self.post(
                self.reverse('board:message_edit', pk=self.message.pk),
                data={'text': 'New text'},
                extra={'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertEqual(response.status_code, 200)
            self.assertIn('{"text": "New text"}', str(response.content))

        msg = Message.objects.get(pk=self.message.pk)
        self.assertEqual(msg.text, 'New text')


class CommentCreateViewTest(TestCase):
    def setUp(self):
        self.user = self.make_user()
        self.message = Message.objects.create(text="Msg for comments", created_by=self.user)

    def test_get_success_url(self):
        view = CBVTestCase().get_instance(views.CommentCreateView)
        self.assertEqual(view.get_success_url(), self.reverse('board:home'))

    def test_login_required(self):
        self.assertLoginRequired('board:comment_add')

    def test_form_valid(self):
        self.assertEqual(Comment.objects.count(), 0)

        with self.login(self.user):
            response = self.post(
                self.reverse('board:comment_add'),
                data={
                    'text': 'Create comment AJAX',
                    'created_by': self.user.pk,
                    'msg': self.message.pk
                },
                extra={'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertEqual(response.status_code, 200)
            self.assertIn('Create comment AJAX', str(response.content))

        self.assertEqual(Comment.objects.count(), 1)


class CommentUpdateViewTest(TestCase):
    def setUp(self):
        self.user = self.make_user()
        self.user2 = self.make_user('user2')
        self.message = Message.objects.create(text="Msg text", created_by=self.user)
        self.comment = Comment.objects.create(text="Old comment", created_by=self.user, msg=self.message)

    def test_get_success_url(self):
        view = CBVTestCase().get_instance(views.CommentUpdateView)
        self.assertEqual(view.get_success_url(), self.reverse('board:home'))

    def test_wrong_user(self):
        with self.login(self.user2):
            response = self.post(
                self.reverse('board:comment_edit', pk=self.comment.pk),
                data={'text': 'New comment'},
                extra={'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertEqual(response.status_code, 404)

    def test_form_valid(self):
        with self.login(self.user):
            response = self.post(
                self.reverse('board:comment_edit', pk=self.comment.pk),
                data={'text': 'New comment'},
                extra={'HTTP_X_REQUESTED_WITH': 'XMLHttpRequest'}
            )
            self.assertEqual(response.status_code, 200)
            self.assertIn('{"comment": "New comment"}', str(response.content))

        comment = Comment.objects.get(pk=self.comment.pk)
        self.assertEqual(comment.text, 'New comment')
