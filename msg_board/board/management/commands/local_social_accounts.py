from django.conf import settings
from django.core.management.base import BaseCommand, CommandError

from allauth.socialaccount.models import SocialApp


class Command(BaseCommand):
    help = 'Loads allauth SocialApp settings into database'

    def handle(self, *args, **options):
        for provider, config in settings.SOCIALACCOUNT_APPS.items():
            try:
                app_exists = SocialApp.objects.filter(client_id=config['client_id']).exists()
                if not app_exists:
                    social_app = SocialApp(provider=provider,
                                           name=config['name'],
                                           client_id=config['client_id'],
                                           secret=config['secret'])
                    social_app.save()
                    social_app.sites = [settings.SITE_ID]
                    social_app.save()

                    print('Successfully created app: {}'.format(social_app))
                else:
                    print('App already created: {}'.format(provider))
            except Exception as e:
                raise CommandError(e)
