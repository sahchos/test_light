# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django import forms
from django.utils.translation import ugettext as _
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Submit, Layout

from .models import Message, Comment


class MessageForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(MessageForm, self).__init__(*args, **kwargs)

        self.helper = FormHelper(self)
        self.helper.form_class = 'form-inline tac'
        self.helper.form_action = 'board:message_add'
        self.helper.layout = Layout(
            'text',
            Submit('submit', _('Send'), css_class='btn btn-primary'),
        )

    class Meta:
        model = Message
        fields = ['text']
        labels = {
            'text': ''
        }
        widgets = {
            'text': forms.Textarea(attrs={
                'cols': 60,
                'rows': 5,
                'placeholder': _('Text message'),
                'required': True
            })
        }


class CommentForm(forms.ModelForm):

    class Meta:
        model = Comment
        fields = ['text', 'msg', 'comment']

    def clean(self):
        cleaned_data = super(CommentForm, self).clean()

        # check that comment from correct thread(msg)
        if cleaned_data.get('comment') and cleaned_data['comment'].msg != cleaned_data['msg']:
            raise forms.ValidationError(_('Incorrect data'))

        return cleaned_data
