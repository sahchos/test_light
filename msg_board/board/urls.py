# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.MessageListView.as_view(), name='home'),
    url(r'^comment/add/$', views.CommentCreateView.as_view(), name='comment_add'),
    url(r'^comment/(?P<pk>[^/]+)/edit/$', views.CommentUpdateView.as_view(), name='comment_edit'),
    url(r'^message/add/$', views.MessageCreateView.as_view(), name='message_add'),
    url(r'^message/(?P<pk>[^/]+)/edit/$', views.MessageUpdateView.as_view(), name='message_edit'),
]
