# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf import settings
from django.db import models
from django.utils.translation import ugettext as _
from django.utils.encoding import python_2_unicode_compatible


class MessageAbstract(models.Model):
    """
    An abstract base class model for Message and Comment
    """
    text = models.TextField(verbose_name=_('Text'))
    created_by = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('Created by'))
    date_created = models.DateTimeField(auto_now_add=True, verbose_name=_('Date created'))
    date_updated = models.DateTimeField(auto_now=True, verbose_name=_('Date updated'))

    class Meta:
        abstract = True


@python_2_unicode_compatible
class Message(MessageAbstract):
    def __str__(self):
        return 'Message {}'.format(self.text[:10])


@python_2_unicode_compatible
class Comment(MessageAbstract):
    """
    Comments could be attached to Message or Comment itself.
    msg used as thread of comments
    """
    msg = models.ForeignKey(Message)
    comment = models.ForeignKey('self', blank=True, null=True, related_name='comments')

    def __str__(self):
        return 'Comment {}'.format(self.text[:10])
