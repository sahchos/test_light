# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django import template

register = template.Library()


@register.filter
def classname(obj):
    return obj.__class__.__name__


@register.filter
def get(dictionary, key):
    return dictionary.get(key)


@register.inclusion_tag('snippets/msg.html', takes_context=True)
def render_msg(context, msg, comments):
    data = context
    data.update({'msg': msg, 'comments': comments})
    return data
