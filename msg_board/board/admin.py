# -*- coding: utf-8 -*-
from __future__ import absolute_import, unicode_literals

from django.contrib import admin

from .models import Message, Comment


@admin.register(Message)
class MessageAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Message._meta.fields]


@admin.register(Comment)
class CommentAdmin(admin.ModelAdmin):
    list_display = [f.name for f in Comment._meta.fields]
