# -*- coding: utf-8 -*-
from __future__ import unicode_literals, absolute_import

from django.conf import settings
from django.core.urlresolvers import reverse
from django.views.generic import CreateView, UpdateView, ListView
from django.http import JsonResponse, Http404
from django.template.loader import render_to_string
from django.template import RequestContext
from django.shortcuts import redirect
from braces.views import LoginRequiredMixin

from .models import Message, Comment
from .forms import MessageForm, CommentForm


class AjaxableResponseMixin(object):
    """
    Mixin to add AJAX support to a form.
    Must be used with an object-based FormView (e.g. CreateView)
    """
    def form_invalid(self, form):
        super(AjaxableResponseMixin, self).form_invalid(form)
        if self.request.is_ajax():
            return JsonResponse(form.errors, status=400)


class MessageListView(ListView):
    model = Message
    template_name = 'board/home.html'
    context_object_name = 'msgs'
    paginate_by = settings.MESSAGE_PER_PAGE

    def get_context_data(self, **kwargs):
        context = super(MessageListView, self).get_context_data(**kwargs)
        context['form'] = MessageForm()

        msg_ids = [obj.id for obj in context['msgs']]
        # collect all needed comments by messages and by comments
        context['comments_all'] = {'by_message': {}, 'by_comment': {}}
        for obj in Comment.objects.select_related('created_by').filter(msg_id__in=msg_ids).order_by('date_created'):
            by_what = 'by_comment' if obj.comment_id else 'by_message'
            object_id = obj.comment_id or obj.msg_id

            if object_id not in context['comments_all'][by_what]:
                context['comments_all'][by_what][object_id] = []
            context['comments_all'][by_what][object_id].append(obj)

        return context

    def get_queryset(self):
        return Message.objects.select_related('created_by').order_by('-date_created')


class MessageCreateView(LoginRequiredMixin, CreateView):
    model = Message
    form_class = MessageForm

    def get_success_url(self):
        return reverse('board:home')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        return super(MessageCreateView, self).form_valid(form)

    def form_invalid(self, form):
        return redirect('board:home')


class MessageUpdateView(LoginRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Message
    fields = ['text']

    def get_success_url(self):
        return reverse('board:home')

    def get_object(self, queryset=None):
        obj = super(MessageUpdateView, self).get_object()
        if not obj.created_by_id == self.request.user.id:
            raise Http404
        return obj

    def form_valid(self, form):
        super(MessageUpdateView, self).form_valid(form)

        return JsonResponse({'text': self.object.text})


class CommentCreateView(LoginRequiredMixin, AjaxableResponseMixin, CreateView):
    model = Comment
    form_class = CommentForm

    def get_success_url(self):
        return reverse('board:home')

    def form_valid(self, form):
        form.instance.created_by = self.request.user
        super(CommentCreateView, self).form_valid(form)

        data = {
            'comment': render_to_string('snippets/msg.html', {
                'msg': self.object,
                'comments': []
            }, RequestContext(self.request))
        }

        return JsonResponse(data)


class CommentUpdateView(LoginRequiredMixin, AjaxableResponseMixin, UpdateView):
    model = Comment
    fields = ['text']

    def get_success_url(self):
        return reverse('board:home')

    def get_object(self, queryset=None):
        obj = super(CommentUpdateView, self).get_object()
        if not obj.created_by_id == self.request.user.id:
            raise Http404
        return obj

    def form_valid(self, form):
        super(CommentUpdateView, self).form_valid(form)

        return JsonResponse({'comment': self.object.text})
