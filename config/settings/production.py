from .base import *

SECRET_KEY = env('DJANGO_SECRET_KEY')
EMAIL_BACKEND = 'core.mail.backends.smtp.EmailBackend'

# other production specific settings
